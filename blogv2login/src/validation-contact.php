<?php

//1-configurer les messages en créant de variables

$message_erreur = 'erreur les champs suivants doivent être complétés';
$message_ok = 'Votre message a bien été envoyé';
$message = $message_erreur;

//2-définir les constances pour l'envoi d'emails
define('MAIL_EXPEDITEUR', 'alexiagenieys@mailo.com');
define('MAIL_SUJET', 'contact blog V1');

//3 vérifier les champs de formulaires
if (empty($_POST['nom'])) {

    $message .= ': Le champs nom est vide <br/>';
}
if (empty($_POST['prenom'])) {
    $message .= ':Le champs prenom est vide <br/>';
}
if (empty($_POST['email'])) {
    $message .= ':Le champs email est vide <br/>';
}
if (empty($_POST['telephone'])) {
    $message .= ':Le champs telephone est vide <br/>';
}
if (empty($_POST['message'])) {
    $message .= ':Le champs message est vide <br/>';
}
//vérifier que les champs sont bien remplis
//si le message final est plus long que le message d'erreur de départ =>erreur affichage de l'erreur 
//on verra le mail uniquement si tous les champs sont remplis
//strlen()=> string et lengh, défini le nombre de caractère dans une chaine

if (strlen($message) > strlen($message_erreur)) {

    echo $message;
}
//sinon on créé une variable pour récupérer le contenu des champs
// en enlevant les espaces et les caractères d'échappement => trim()/stripslashes

else {
    foreach ($_POST as $index => $valeur) {
        // $$index=$valeur
        $$index = stripslashes(trim($valeur));
    }
}

//Préparation de l'entête du mail
$mail_entete = "MIME-Version: 1.0\r\n";
$mail_entete .= "From:{$_POST['nom']}" . "<{$_POST['email']}>\r\n";
$mail_entete .= 'Reply-To:' . $_POST['email'] . "\r\n";
$mail_entete .= 'Content-Type: text/plain;charset="UTF-8"';
$mail_entete .= "\r\nContent-Transfert-Encoding:8bit\r\n";
$mail_entete .= 'X-Mailer:PHP/' . phpversion() . "\r\n";

//Préparation du corps du mail

$mail_corps = "Message de : $nom - $prenom \r\n";
$mail_corps .= "Email : $email\r\n";
$mail_corps .= "telephone : $telephone\r\n";
$mail_corps .= "message : $message\r\n";

//Envoie du mail

if (mail(MAIL_EXPEDITEUR, MAIL_SUJET, $mail_corps, $mail_entete)) {
    //si on a tout cela alors le message est ok et envoie du mail

    echo '<script type="text/javascript">
    alert("votre email a bien été envoyé");
    window.location="../www/index.php";
    </script>';
} else {
    // Le mail n'a pas été envoyé

    echo 'erreur';
}
