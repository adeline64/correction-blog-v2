
<?php


if (isset($_GET['id']) === false) {
    trigger_error('article inconnu', E - USER_ERROR);
}

$article_id = $_GET['id'];

if (array_key_exists($article_id, $articles) === false) {
    header('Location:404.php');
}


$article = $articles[$article_id];

//on l'affiche

$page_title = $article['title'];
$page_content = $article['content'];
$page_content .= '<br><a href="index.php">Retour vers la page d\'acceuil</a>';



?>
