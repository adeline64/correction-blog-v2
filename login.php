<?php

include (__DIR__) . '/src/init.php';

$error_message = '';

//verification des champs

if (empty($_POST) === false) {

    //on stocke ces donnéees dans des variables

    $post_username = $_POST['user_login'];
    $post_password = $_POST['user_pass'];

    //on stocke vérifie que les champs correspondent aux données existantes (json)

    if ($post_username == $json_data['username']) {

        if ($post_password == $json_data['password']) {

            //on connecte l'utilisateur (on l'enregistre dans une session)

            $_SESSION['user'] = $post_username;

            //on redirige vers l'index

            header('Location:index.php');
            exit();
        } else {
            $error_message = 'erreur de mot de passe';
        }
    } else {
        $error_message = 'Erreur de pseudo';
    }
}

// l'Affichage

$page_title = '<h1>Connexion</h1><br>';

//  s'il y a messge d'erreur 

if (empty($error_message) == false) {

    //on affiche le message d'erreur
    $error_message = "<div class=\"alert alert-danger\">{$error_message}</div>";
}


$page_content = <<<LOGIN
<form action="" method="post" class="col-md-6 offset-md-3">
  $error-message
    <input type="text" name="user_login" placeholder="votre pseudo" class="form-control" required>
    <input type="password" name="user_pass" placeholder="votre mot de passe"  class="form-control" required>
    <button type="Submit" class="btn btn-primary"> Se connecter</button>
</form>
LOGIN;
