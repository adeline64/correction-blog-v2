<!DOCTYPE html>
<html>
<?php

//affichage des erreurs
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

//démarrer une session

session_start();


// chargement du contenu avec json
//1 -on définir le chemin. on stocke le chemin dans la variable $json_file_path

$json_file_path = (__DIR__) . '/articles.json';

//on vérifie que le fichier existe bien à cet emplacement (debug)

//var_dump(file_exists($json_file_path));
//exit();

//on vérifie que le fichier existe

if (file_exists($json_file_path) === false) {
    //on lance une exception
    throw new Exception('Fichier Introuvable');
}
//on récupère le contenudu fichier json
$json_file_content = file_get_contents($json_file_path);

//var_dump($json_file_content)
//on transforme le contenu json en tableau avec json_decode
$json_data = json_decode($json_file_content, true);
//on vérifie si les données json récupérées sont valides
if (json_last_error() !== JSON_ERROR_NONE) {
    throw new Exception('Erreur Json "%s"', json_last_error_msg());
}
//On extrait la variable article
$articles = $json_data['articles'];

?>

</html>